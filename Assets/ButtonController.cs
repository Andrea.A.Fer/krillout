
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonController : MonoBehaviour
{
    public TMP_Text Name;
    public TMP_Text Descripcion;
    public Image Habitat;
    public Image Pez;
    public TMP_Text PH;
    public TMP_Text Temperature;
    public TMP_Text PH2;
    public TMP_Text Temperature2;

    public void MostrarValores(Educativo pez)
    {
        // Mostrar los valores del pez en los componentes UI
        Name.text = pez.Name;
        Descripcion.text = pez.Descripcion;
        Habitat.sprite = pez.Habitat;
        Pez.sprite = pez.Pez;
        PH.text = "PH: " + pez.PH.ToString();
        Temperature.text = "Temperatura: " + pez.Temperature.ToString();
        Temperature2.text =  pez.Temperature2.ToString();
        PH2.text =   pez.PH2.ToString();
    }
}


