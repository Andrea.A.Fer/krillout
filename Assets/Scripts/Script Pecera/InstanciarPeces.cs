using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciarPeces : MonoBehaviour
{
    public List<GameObject> fishPrefabsSaltwater = new List<GameObject>(); // Lista de prefabs de peces de agua salada
    public List<GameObject> fishPrefabsFreshwater = new List<GameObject>(); // Lista de prefabs de peces de agua dulce
    public float tankSize = 0.2f;

    public List<GameObject> allFish = new List<GameObject>();

    public Vector3 goalPos = Vector3.zero;

    public int currentFishCount = 0;

    public void InstantiatedAddFish(List<GameObject> fishPrefabs)
    {
        foreach (GameObject fishPrefab in fishPrefabs)
        {
            Vector3 pos = new Vector3(Random.Range(-tankSize, tankSize),
                                      Random.Range(-tankSize, tankSize),
                                      Random.Range(-tankSize, tankSize));
            GameObject newFish = Instantiate(fishPrefab, pos, Quaternion.identity);
            allFish.Add(newFish);
            currentFishCount++;
        }
    }

    void Update()
    {
        if (Random.Range(0, 0.2f) < 1f)
        {
            goalPos = new Vector3(Random.Range(-tankSize, tankSize),
                                  Random.Range(-tankSize, tankSize),
                                  Random.Range(-tankSize, tankSize));
        }
    }
}
