using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class QuizManager : MonoBehaviour
{
    public List<QuestionAndAnswers> QnA;
    private List<QuestionAndAnswers> originalQnA; // Variable para almacenar una copia de respaldo de las preguntas originales

    public GameObject[] options;
    public int currentQuestion;
    public GameObject panelToClose; // Asigna esto en el Inspector
    public Button playButton;
    public TextMeshProUGUI QuestionTxt;

    private void Start()
    {
        // Hacer una copia de respaldo de las preguntas originales
        originalQnA = new List<QuestionAndAnswers>(QnA);

        generateQuestion();
    }

    public void correct()
    {
        QnA.RemoveAt(currentQuestion);
        generateQuestion();

        // Busca la instancia de MoneyManager y agrega 10 monedas
        MoneyManager moneyManager = FindObjectOfType<MoneyManager>();
        if (moneyManager != null)
        {
            moneyManager.AddCoins(200);
        }
    }

    public void wrong()
    {
        QnA.RemoveAt(currentQuestion);
        generateQuestion();
    }

    void SetAnswers()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<AnswerScript>().isCorrect = false;
            options[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = QnA[currentQuestion].Answers[i];

            if (QnA[currentQuestion].CorrectAnswer == i + 1)
            {
                options[i].GetComponent<AnswerScript>().isCorrect = true;
            }
        }
    }

    void generateQuestion()
    {
        if (QnA.Count > 0)
        {
            currentQuestion = Random.Range(0, QnA.Count);
            QuestionTxt.text = QnA[currentQuestion].Question;
            SetAnswers();
        }
        else
        {
            Debug.Log("Out of Questions");

            // Restaurar la lista de preguntas original
            QnA = new List<QuestionAndAnswers>(originalQnA);

            // Volver a generar una pregunta
            generateQuestion();

            ClosePanel();
        }
    }

    public void ClosePanel()
    {
        if (panelToClose != null)
        {
            panelToClose.SetActive(false);
        }
    }
}
