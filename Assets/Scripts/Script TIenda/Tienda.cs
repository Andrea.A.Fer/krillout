using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tienda : MonoBehaviour
{
    public int[] saltwaterFishPrices = { 20, 25, 30, 50 }; // Precios de los peces de agua salada
    public int[] freshwaterFishPrices = { 15, 18, 22 }; // Precios de los peces de agua dulce
    public Button[] buySaltwaterFishButtons; // Botones para comprar peces de agua salada
    public Button[] buyFreshwaterFishButtons; // Botones para comprar peces de agua dulce
    public Button feedFishButton; // Bot�n para dar de comer a los peces
    public GameObject fishFoodObject; // Objeto para las part�culas de comida para los peces

    public MoneyManager moneyManager; // Referencia al script MoneyManager

    public ControladorPecesTienda controladorPecesTienda;

    private int feedCount = 0; // Contador de veces que el jugador ha dado de comer a los peces

    void Start()
    {

        feedFishButton.onClick.AddListener(FeedFish);

    }

    
    public void BuySaltwaterFish(int fishType)
    {
        int price = saltwaterFishPrices[fishType]; // Obtener el precio del tipo de pez seleccionado
        if (moneyManager.CanAfford(price))
        {
            moneyManager.SubtractCoins(price); // Restar el precio del pez de los euros del jugador
            // Aqu� puedes a�adir el c�digo para a�adir el pez de agua salada al juego
        }
        UpdateBuyButtonStates(); // Actualizar el estado de los botones de compra despu�s de la compra
    }

    public void BuyFreshwaterFish(int fishType)
    {
        int price = freshwaterFishPrices[fishType]; // Obtener el precio del tipo de pez seleccionado
        if (moneyManager.CanAfford(price))
        {
            moneyManager.SubtractCoins(price); // Restar el precio del pez de los euros del jugador
            // Aqu� puedes a�adir el c�digo para a�adir el pez de agua dulce al juego
        }
        UpdateBuyButtonStates(); // Actualizar el estado de los botones de compra despu�s de la compra
    }
    

    

    void OnEnable()
    {
        MoneyManager.OnMoneyChanged += UpdateBuyButtonStates; // Suscribirse al evento.
    }

    void OnDisable()
    {
        MoneyManager.OnMoneyChanged -= UpdateBuyButtonStates; // Anular la suscripci�n al evento.
    }

    private void UpdateBuyButtonStates()
    {
        // Actualizar botones de peces de agua salada
        for (int i = 0; i < buySaltwaterFishButtons.Length; i++)
        {
            buySaltwaterFishButtons[i].interactable = moneyManager.CanAfford(saltwaterFishPrices[i]);
        }

        // Actualizar botones de peces de agua dulce
        for (int i = 0; i < buyFreshwaterFishButtons.Length; i++)
        {
            buyFreshwaterFishButtons[i].interactable = moneyManager.CanAfford(freshwaterFishPrices[i]);
        }

        // Actualizar el estado del bot�n de alimentaci�n
        feedFishButton.interactable = moneyManager.CanAfford(10) && feedCount < 2;
    }

    /*
    public void FeedFish()
    {
        if (moneyManager.CanAfford(10)) // Verificar si el jugador tiene suficientes euros
        {
            moneyManager.SubtractCoins(10); // Restar 10 euros del jugador

            // Activar el objeto de comida para los peces
            fishFoodObject.SetActive(true);
            Instantiate(fishFoodObject, FishTankManager.Singleton.GetPointInsideFishTank(), fishFoodObject.transform.rotation);
            Debug.Log("Comida");

            feedCount++; // Incrementar el contador de veces que el jugador ha dado de comer a los peces

            if (feedCount == 2)
            {
                feedFishButton.interactable = false;
                StartCoroutine(EnableFeedButtonAfterSeconds(20));
            }
            else
            {
                UpdateBuyButtonStates();  // Actualizar despu�s de cada interacci�n para el caso de no tener suficiente dinero
            }
        }
    }
    */

    public void FeedFish()
    {
        if (moneyManager.CanAfford(10)) // Verificar si el jugador tiene suficientes euros
        {
            moneyManager.SubtractCoins(10); // Restar 10 euros del jugador

            // Activar el objeto de comida para los peces
            fishFoodObject.SetActive(true);
            GameObject foodInstance = Instantiate(fishFoodObject, FishTankManager.Singleton.GetPointInsideFishTank(), Quaternion.identity);
            Debug.Log("Comida");

            // Inicia la coroutine para remover la comida despu�s de 5 segundos
            StartCoroutine(RemoveFoodAfterSeconds(foodInstance, 20f));

            feedCount++; // Incrementar el contador de veces que el jugador ha dado de comer a los peces

            if (feedCount == 2)
            {
                feedFishButton.interactable = false;
                StartCoroutine(EnableFeedButtonAfterSeconds(20));
            }
            else
            {
                UpdateBuyButtonStates();  // Actualizar despu�s de cada interacci�n para el caso de no tener suficiente dinero
            }
        }
    }
    // Coroutine para habilitar el bot�n de dar de comer a los peces despu�s de un cierto n�mero de segundos
    private IEnumerator EnableFeedButtonAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        feedFishButton.interactable = true; // Habilitar el bot�n
        feedCount = 0; // Resetear el contador de veces que el jugador ha dado de comer a los peces
    }

    private IEnumerator RemoveFoodAfterSeconds(GameObject food, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(food); // O food.SetActive(false); si prefieres reutilizarlo
    }
}
/*
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tienda : MonoBehaviour
{
    public int[] saltwaterFishPrices = { 20, 25, 30 }; // Precios de los peces de agua salada
    public int[] freshwaterFishPrices = { 15, 18, 22 }; // Precios de los peces de agua dulce
    public Button[] buySaltwaterFishButtons; // Botones para comprar peces de agua salada
    public Button[] buyFreshwaterFishButtons; // Botones para comprar peces de agua dulce
    public Button feedFishButton; // Bot�n para dar de comer a los peces
    public GameObject fishFoodObject; // Objeto para las part�culas de comida para los peces

    public MoneyManager moneyManager; // Referencia al script MoneyManager

    private int feedCount = 0; // Contador de veces que el jugador ha dado de comer a los peces

    void Start()
    {

        feedFishButton.onClick.AddListener(FeedFish);
        
    }

    public void BuySaltwaterFish(int fishType)
    {
        int price = saltwaterFishPrices[fishType]; // Obtener el precio del tipo de pez seleccionado
        if (moneyManager.CanAfford(price))
        {
            moneyManager.SubtractCoins(price); // Restar el precio del pez de los euros del jugador
            // Aqu� puedes a�adir el c�digo para a�adir el pez de agua salada al juego
        }
        UpdateBuyButtonStates(); // Actualizar el estado de los botones de compra despu�s de la compra
    }

    public void BuyFreshwaterFish(int fishType)
    {
        int price = freshwaterFishPrices[fishType]; // Obtener el precio del tipo de pez seleccionado
        if (moneyManager.CanAfford(price))
        {
            moneyManager.SubtractCoins(price); // Restar el precio del pez de los euros del jugador
            // Aqu� puedes a�adir el c�digo para a�adir el pez de agua dulce al juego
        }
        UpdateBuyButtonStates(); // Actualizar el estado de los botones de compra despu�s de la compra
    }

    void OnEnable()
    {
        MoneyManager.OnMoneyChanged += UpdateBuyButtonStates; // Suscribirse al evento.
    }

    void OnDisable()
    {
        MoneyManager.OnMoneyChanged -= UpdateBuyButtonStates; // Anular la suscripci�n al evento.
    }

    private void UpdateBuyButtonStates()
    {
        // Actualizar botones de peces de agua salada
        for (int i = 0; i < buySaltwaterFishButtons.Length; i++)
        {
            buySaltwaterFishButtons[i].interactable = moneyManager.CanAfford(saltwaterFishPrices[i]);
        }

        // Actualizar botones de peces de agua dulce
        for (int i = 0; i < buyFreshwaterFishButtons.Length; i++)
        {
            buyFreshwaterFishButtons[i].interactable = moneyManager.CanAfford(freshwaterFishPrices[i]);
        }

        // Actualizar el estado del bot�n de alimentaci�n
        feedFishButton.interactable = moneyManager.CanAfford(10) && feedCount < 2;
    }

    public void FeedFish()
    {
        if (moneyManager.CanAfford(10)) // Verificar si el jugador tiene suficientes euros
        {
            moneyManager.SubtractCoins(10); // Restar 10 euros del jugador

            // Activar el objeto de comida para los peces
            fishFoodObject.SetActive(true);
            Instantiate(fishFoodObject, FishTankManager.Singleton.GetPointInsideFishTank(), fishFoodObject.transform.rotation);
            Debug.Log("Comida");

            feedCount++; // Incrementar el contador de veces que el jugador ha dado de comer a los peces

            if (feedCount == 2)
            {
                feedFishButton.interactable = false;
                StartCoroutine(EnableFeedButtonAfterSeconds(20));
            }
            else
            {
                UpdateBuyButtonStates();  // Actualizar despu�s de cada interacci�n para el caso de no tener suficiente dinero
            }
        }
    }

    // Coroutine para habilitar el bot�n de dar de comer a los peces despu�s de un cierto n�mero de segundos
    private IEnumerator EnableFeedButtonAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        feedFishButton.interactable = true; // Habilitar el bot�n
        feedCount = 0; // Resetear el contador de veces que el jugador ha dado de comer a los peces
    }
}
*/