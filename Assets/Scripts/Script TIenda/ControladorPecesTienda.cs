using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPecesTienda : MonoBehaviour
{
    public InstanciarPeces fishManager; // Referencia al script InstanciarPeces
    public GameObject[] saltwaterFishPrefabs; // Array de prefabs de peces de agua salada
    public GameObject[] freshwaterFishPrefabs; // Array de prefabs de peces de agua dulce

    // M�todo que se llamar� cuando se presione el bot�n de comprar pez de agua salada
    public void OnSaltwaterFishButtonClick(int fishType)
    {
        if (fishType < saltwaterFishPrefabs.Length)
        {
            fishManager.InstantiatedAddFish(new List<GameObject>() { saltwaterFishPrefabs[fishType] }); // Llama al m�todo InstantiatedAddFish del script InstanciarPeces pasando el prefab de pez de agua salada
            Debug.Log("Se llama al script Controlador Peces Tienda para comprar pez de agua salada.");
        }
        else
        {
            Debug.LogError("Tipo de pez de agua salada no v�lido. Aseg�rate de tener suficientes prefabs de peces de agua salada asignados.");
        }
    }

    // M�todo que se llamar� cuando se presione el bot�n de comprar pez de agua dulce
    public void OnFreshwaterFishButtonClick(int fishType)
    {
        if (fishType < freshwaterFishPrefabs.Length)
        {
            fishManager.InstantiatedAddFish(new List<GameObject>() { freshwaterFishPrefabs[fishType] }); // Llama al m�todo InstantiatedAddFish del script InstanciarPeces pasando el prefab de pez de agua dulce
            Debug.Log("Se llama al script Controlador Peces Tienda para comprar pez de agua dulce.");
        }
        else
        {
            Debug.LogError("Tipo de pez de agua dulce no v�lido. Aseg�rate de tener suficientes prefabs de peces de agua dulce asignados.");
        }
    }
}