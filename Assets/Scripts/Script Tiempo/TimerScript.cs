using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

public class TimerScript : MonoBehaviour
{
    public string myFormat;

    public TextMeshProUGUI mainClock;

    public System.TimeSpan timeSpan = new System.TimeSpan(0, 0, 0, 0);

    public System.DateTime date = new System.DateTime(2024, 03, 09);

    public float timeRate = 1;

    private void Update()
    {
        float miliseconds = Time.deltaTime * 1000 * timeRate;

        timeSpan += new System.TimeSpan(0, 0, 0, 0, (int)miliseconds);
        System.DateTime dateTime = date.Add(timeSpan);

        mainClock.text = dateTime.ToString(@myFormat, new CultureInfo("en-US"));
    }

    public void AddTime(int value)
    {
        timeSpan += new System.TimeSpan(value, 0, 0);
    }
}
