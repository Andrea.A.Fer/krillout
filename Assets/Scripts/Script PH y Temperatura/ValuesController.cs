using UnityEngine;
using TMPro;
using System.Collections;

public class ValuesController : MonoBehaviour
{
    public TMP_Text pHText;
    public TMP_Text temperatureText;
    private int pH = 7; // Valor inicial de pH
    private float temperature = 25.0f; // Valor inicial de temperatura

    void Start()
    {
        UpdateTexts();
        // Inicia una corutina que espera 20 segundos antes de iniciar las corutinas de cambio
        StartCoroutine(StartAfterDelay());
    }

    IEnumerator StartAfterDelay()
    {
        // Espera 20 segundos antes de iniciar las corutinas de cambio
        yield return new WaitForSeconds(20);

        // Inicia las corutinas para cambiar los valores de pH y temperatura
        StartCoroutine(ChangePH());
        StartCoroutine(ChangeTemperature());
    }

    void UpdateTexts()
    {
        // Actualiza el texto y color para el pH
        pHText.text = "pH: " + pH.ToString("F1");
        if (pH == 7)
        {
            pHText.color = Color.green;
        }
        else
        {
            pHText.color = Color.red;
        }

        // Actualiza el texto y color para la temperatura
        temperatureText.text = "Temperatura: " + temperature.ToString("F1") + "�C";
        if (Mathf.Approximately(temperature, 25.0f))
        {
            temperatureText.color = Color.green;
        }
        else
        {
            temperatureText.color = Color.red;
        }
    }

    IEnumerator ChangePH()
    {
        while (true)
        {
            // Cambia el valor del pH aqu�
            pH += 1; // Modificar seg�n la l�gica necesaria
            UpdateTexts();
            yield return new WaitForSeconds(20);
        }
    }

    IEnumerator ChangeTemperature()
    {
        while (true)
        {
            // Cambia el valor de la temperatura aqu�
            temperature += 1.0f; // Modificar seg�n la l�gica necesaria
            UpdateTexts();
            yield return new WaitForSeconds(20);
        }
    }

    public void IncrementPH()
    {
        pH += 1;
        UpdateTexts();
    }

    public void DecrementPH()
    {
        pH -= 1;
        UpdateTexts();
    }

    public void IncrementTemperature()
    {
        temperature += 1.0f;
        UpdateTexts();
    }

    public void DecrementTemperature()
    {
        temperature -= 1.0f;
        UpdateTexts();
    }
}