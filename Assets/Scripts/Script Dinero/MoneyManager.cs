/*
using System;
using TMPro;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    public static event Action OnMoneyChanged; // Evento que se dispara cuando el dinero cambia.

    public int initialCoins = 450;
    public TextMeshProUGUI coinsText;

    private int coins;

    void Start()
    {
        coins = initialCoins;
        UpdateCoinsText();
    }

    private void UpdateCoinsText()
    {
        coinsText.text = $"Monedas: {coins}";
        OnMoneyChanged?.Invoke();  // Disparar el evento cuando el texto de las monedas se actualiza.
    }

    public bool CanAfford(int amount)
    {
        return coins >= amount;
    }

    public void SubtractCoins(int amount)
    {
        if (CanAfford(amount))
        {
            coins -= amount;
            UpdateCoinsText();
        }
    }

    public void AddCoins(int amount)
    {
        coins += amount;
        UpdateCoinsText();
    }
}



//CODIGO QUE SE EMPLEAR� AL FINAL PARA EL DINERO DEL JUGADOR IMPORTANTE NO BORRAR 
using System.Collections;
using TMPro;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    public int defaultCoins = 450; // Cantidad de monedas por defecto si no hay datos guardados
    public TextMeshProUGUI coinsText;

    private int coins;

    void Start()
    {
        LoadCoins();
        UpdateCoinsText();
    }

    private void LoadCoins()
    {
        coins = PlayerPrefs.GetInt("coins", defaultCoins);
    }

    private void SaveCoins()
    {
        PlayerPrefs.SetInt("coins", coins);
        PlayerPrefs.Save();
    }

    public void UpdateCoinsText()
    {
        if (coinsText != null)
        {
            coinsText.text = "Monedas: " + coins;
        }
    }

    public bool CanAfford(int amount)
    {
        return coins >= amount;
    }

    public void SubtractCoins(int amount)
    {
        if (CanAfford(amount))
        {
            coins -= amount;
            SaveCoins();
            UpdateCoinsText();
        }
    }

    public void AddCoins(int amount)
    {
        coins += amount;
        SaveCoins();
        UpdateCoinsText();
    }
}
*/

using System;
using TMPro;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    public static event Action OnMoneyChanged; // Evento para notificar cambios en el dinero

    public int defaultCoins = 450; // Cantidad de monedas por defecto
    public TextMeshProUGUI coinsText;
    private int coins;

    void Start()
    {
        LoadCoins();
        UpdateCoinsText();
    }

    private void LoadCoins()
    {
        coins = PlayerPrefs.GetInt("coins", defaultCoins);
    }

    private void SaveCoins()
    {
        PlayerPrefs.SetInt("coins", coins);
        PlayerPrefs.Save();
    }

    public void UpdateCoinsText()
    {
        if (coinsText != null)
        {
            coinsText.text = "Monedas: " + coins;
        }
    }

    public bool CanAfford(int amount)
    {
        return coins >= amount;
    }

    public void SubtractCoins(int amount)
    {
        if (CanAfford(amount))
        {
            coins -= amount;
            SaveCoins();
            UpdateCoinsText();
            OnMoneyChanged?.Invoke(); // Disparar el evento
        }
    }

    public void AddCoins(int amount)
    {
        coins += amount;
        SaveCoins();
        UpdateCoinsText();
        OnMoneyChanged?.Invoke(); // Disparar el evento
    }

    public void ResetCoinsToDefault()
    {
        coins = defaultCoins; // Restablece las monedas al valor predeterminado
        SaveCoins(); // Guarda el cambio en PlayerPrefs
        UpdateCoinsText(); // Actualiza el texto de la UI
        OnMoneyChanged?.Invoke(); // Notifica a los suscriptores del cambio
    }
}