
using UnityEngine;

public class MostrarPanelEducacion : MonoBehaviour
{
    public GameObject paneleducativo;

    public void MostrarOcultarPanel()
    {
        // Alternar la visibilidad del panel educativo
        paneleducativo.SetActive(!paneleducativo.activeSelf);
    }
}

